# Demo Blog in Vanilla PHP

A basic blogging platform created to demonstrate my development abilities in Vanilla PHP without any frameworks or libraries.

I started working on this very basic yet functional website 3 weeks ago and I have developed it infrequently in-between a busy time at work.

It implements the MVC pattern (inspired from Laravel and Rails) without relying on external packages or libraries: 

*  Models are representations of data stored in a database accessible using typical ORM methods.
*  Views are logically-separated parts of the user interface which displays information from Models to the user.
*  Controllers are invokable methods associated with URL routes which process the user's request, retrieves the appropriate data and responds with the completed view.

## Directory Structure

For Models, see the `src/models` directory.   
For Views, see the `src/views` directory.   
For Controller methods and their URL routes, see the `routes.php` file.   
For static files, see the `public` directory.    

## See this website live!

http://demo-blog.martinshaw.co/

## Setup

Copy `config.example.php` to `config.php`, then change the database values to match your database environment.   

## Future Development

In the future, I will also implement this same 'Demo Blog' project using the following combinations of languages and frameworks:
   
[Laravel (with Blade Templating)](https://gitlab.com/martinshaw/demo-blog-in-laravel-php)
   
[Nuxt.js (with Vue.js)](https://gitlab.com/martinshaw/demo-blog-in-nuxt-vue)