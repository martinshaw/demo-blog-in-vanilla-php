<?php
namespace App;

class Config extends Singleton
{
	use FetchesArray;

	protected $path = '../config.php';

	protected $settings;

	public function __construct() {
		if (file_exists($this->path) === false) {
			$exception = new \Exception('There is no config file. You may need to copy the \'config.example.php\' file as \'config.php\'');
			echo $exception->getMessage();
			throw $exception;
		}
		$this->settings = $this->fetchArray($this->path);
	}

	public function get($name, $default = null) {
		return isset($this->settings[$name]) ? $this->settings[$name] : $default;
	}
}