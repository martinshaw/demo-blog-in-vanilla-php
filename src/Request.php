<?php
namespace App;

class Request extends Singleton
{
	protected $rawPath;
	protected $path;
	protected $previousUrl;
	protected $method;

	public function __construct() {
		$this->rawPath = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$this->path = parse_url($this->rawPath);
		$this->previousUrl = empty($_SERVER["HTTP_REFERER"]) ? null : parse_url($_SERVER["HTTP_REFERER"]);
		
		$this->method = $_SERVER['REQUEST_METHOD'];
		if (isset($_POST['_method'])) {
			$this->method = strtoupper($_POST['_method']);
			unset($_POST['_method']);
		}
	}

	public function getPath() {
		return $this->path;
	}

	public function getPreviousUrl() {
		return $this->previousUrl;
	}

	public function getMethod() {
		return $this->method;
	}
}