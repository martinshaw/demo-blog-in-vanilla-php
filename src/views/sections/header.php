    <header>
        <nav class="header__navbar">
            <div class="header__navbar--left">
                <a class="header__navbar__item header__navbar__item--logo" href="/">Demo Blog in Vanilla PHP</a>
            </div>
            <div class="header__navbar--right">
                <a class="header__navbar__item" href="/about">About</a>
                <?php if ($auth->isSignedIn()) { ?>
                    <a class="header__navbar__item" href="/posts/new">New Post</a>
                    <a class="header__navbar__item" href="/auth/signout">Sign Out as <?php echo $user->getProperties()['name']; ?></a>
                <?php } else { ?>
                    <a class="header__navbar__item" href="/auth/signin">Sign In</a>
                <?php } ?>
            </div>
        </nav>
        <!-- <section class="header__heading">
            <div class="header__heading__tagline">A basic blogging platform created to demonstrate my development abilities in Vanilla PHP without any frameworks or libraries.</div>
        </section> -->
    </header>