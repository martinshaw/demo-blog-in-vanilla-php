    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo Blog in Vanilla PHP</title>

    <link rel="prefetch" href="/img/photo-1591557132355-52f293ac9722.jpg">

    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300;400;600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/global.css">