<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/index.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island container__island--with_sizebar">
            <section class="posts__container__posts">
                <?php if (empty($data['items'])) { ?>
                    <h1 class="post_list__empty_state">There are no posts to be displayed. Sign In and create a new post!</h1>
                <?php } ?>
                <?php foreach ($data['items'] as $post) { ?>
                <a class="post_list__item" href="/posts/<?php echo $post->getProperties()['id']; ?>">
                    <div class="post_list__item__title"><?php echo $post->getProperties()['title']; ?></div>
                    <div class="post_list__item__details"><?php echo $post->getDetails(); ?></div>
                </a>
                <?php } ?>

                <?php if (isset($data['pagination'])) { ?>
                <div class="post_list__pagination">
                    <a href="<?php echo $data['pagination']['prev']; ?>"><button class="post_list__pagination__button--previous" <?php echo is_null($data['pagination']['prev']) ? 'disabled' : ''; ?>>&larr; Previous</button></a>
                    <div class="post_list__pagination__button--page_number">Page <?php echo $data['pagination']['page']; ?></div>
                    <a href="<?php echo $data['pagination']['next']; ?>"><button class="post_list__pagination__button--next" <?php echo is_null($data['pagination']['next']) ? 'disabled' : ''; ?>>Next &rarr;</button></a>
                </div>
                <?php } ?>
            </section>
            <section class="posts__container__tags">
                <div class="tags__title">
                    Posts by month:
                </div>
                <?php foreach ($data['monthlyPosts'] as $monthYear => $posts) { ?>
                    <?php list($month, $year) = explode(' ', $monthYear); ?>
                    <a class="tags__item--link" href="/posts/<?php echo $month; ?>/<?php echo $year; ?>">
                        <div class="tags__item">
                            <?php echo $monthYear; ?>
                            <div class="tags__item__count"><?php echo count($posts); ?></div>
                        </div>
                    </a>
                <?php } ?>
            </section>
        </div>
    </div>
</body>
</html>