<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/post-new.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <?php if (isset($data['errors'])) { ?>
                <div class="errors">
                    <ul>
                        <?php foreach ($data['errors'] as $error) { ?>
                            <li><?php echo $error; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form action="/posts" method="POST">
                <div class="post__buttons">
                    <input type="submit" value="Save Post &rarr;" />
                    <button onclick="event.preventDefault(); document.location='/posts'; ">Discard Post</button>
                </div>
                <input type="text" class="post__title" name="title" placeholder="Post Title" />
                <textarea type="text" class="post__content" name="content" rows="25" placeholder="Post Content"></textarea>
            </form>
        </div>
    </div>
</body>
</html>