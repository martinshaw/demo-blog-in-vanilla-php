<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/error.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <div class="error__title">404</div>
            <div class="error__details">Requested Page Cannot Be Found</div>
        </div>
    </div>
</body>
</html>