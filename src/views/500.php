<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/error.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <div class="error__title">Error</div>
            <div class="error__details">A server error occurred when attempting to perform your request</div>
            <?php if ($data['debug']) { ?>
            <div class="error__content">
                <p><?php echo $data['exception']->getMessage(); ?></p>
            </div>
            <?php } ?>
        </div>
    </div>
</body>
</html>