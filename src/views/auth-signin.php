<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/auth-signin.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <?php if (isset($data['errors'])) { ?>
                <div class="errors">
                    <ul>
                        <?php foreach ($data['errors'] as $error) { ?>
                            <li><?php echo $error; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form action="/auth/signin" method="POST">
                <div class="form__section">
                    <label for="username">Username: </label>
                    <input type="text" name="username" id="username" placeholder="Username" value="demo" />
                </div>
                
                <div class="form__section">
                    <label for="password">Password:</label>
                    <input type="text" name="password" id="password" placeholder="Password" value="demo" />
                </div>
                
                <div class="form__section">
                    <input type="hidden" name="redirectTo" value="<?php echo $data['redirectTo']; ?>" />
                    <input type="submit" value="Sign In &rarr;" />
                </div>
            </form>
        </div>
    </div>
</body>
</html>