<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/post-new.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <?php if (isset($data['errors'])) { ?>
                <div class="errors">
                    <ul>
                        <?php foreach ($data['errors'] as $error) { ?>
                            <li><?php echo $error; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <form action="/posts/<?php echo $data['post']->getProperties()['id']; ?>" method="POST">
                <div class="post__buttons">
                    <input type="submit" value="Update Post &rarr;" />
                    <button onclick="event.preventDefault(); document.location='/posts/<?php echo $data['post']->getProperties()['id']; ?>'; ">Discard Changes</button>
                </div>
                <input type="hidden" name="_method" value="put" />
                <input type="text" class="post__title" name="title" placeholder="Post Title" value="<?php echo $data['post']->getProperties()['title']; ?>" />
                <textarea type="text" class="post__content" name="content" rows="25" placeholder="Post Content"><?php echo str_replace('<br/>', "\n", base64_decode($data['post']->getProperties()['content'])); ?></textarea>
            </form>
        </div>
    </div>
</body>
</html>