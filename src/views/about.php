<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/about.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <div class="about__title">About Demo Blog in Vanilla PHP</div>
            <div class="about__content">

                <p>
                    A basic blogging platform created to demonstrate my development abilities in Vanilla PHP without any frameworks or libraries.
                </p>

                <p>
                    I started working on this very basic yet functional website 3 weeks ago and I have developed it infrequently in-between a busy time at work.
                </p>

                <p>
                    It implements the MVC pattern (inspired from Laravel and Rails) :

                    <ul style="list-style: disc;">
                        <li>Models are representations of data stored in a database accessible using typical ORM methods. See the <code style="font-family: monospace !important;">src/models</code> directory.</li>
                        <li>Views are logically-separated parts of the user interface which displays information from Models to the user. See the <code style="font-family: monospace !important;">src/views</code> directory.</li>
                        <li>Controllers are invokable methods associated with URL routes which process the user's request, retrieves the appropriate data and responds with the completed view. See the <code style="font-family: monospace !important;">routes.php</code> file.</li>
                    </ul>
                </p>

            </div>
        </div>
    </div>
</body>
</html>