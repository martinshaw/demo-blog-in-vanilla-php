<!DOCTYPE html>
<html lang="en">
<head>
    <?php $section('head.php'); ?>
    <link rel="stylesheet" href="/css/post.css">
</head>
<body>
    <?php $section('header.php'); ?>

    <div class="container">
        <div class="container__island">
            <?php if ($auth->isSignedIn()) { ?>
                <div class="post__buttons">
                    <button onclick="event.preventDefault(); document.location='/posts/<?php echo $data['post']->getProperties()['id']; ?>/edit'; ">Edit Post</button>
                    <button onclick="event.preventDefault(); document.location='/posts/<?php echo $data['post']->getProperties()['id']; ?>/delete'; ">Delete Post</button>
                </div>
            <?php } ?>
            <div class="post__title"><?php echo $data['post']->getProperties()['title']; ?></div>
            <div class="post__details"><?php echo $data['post']->getDetails(); ?></div>
            <div class="post__content"><?php echo base64_decode($data['post']->getProperties()['content']); ?></div>
        </div>
    </div>
</body>
</html>