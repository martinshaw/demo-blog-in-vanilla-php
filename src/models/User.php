<?php
namespace App\Models;

class User extends Model
{
	public static $table = 'users';

	public static $fields = [
		'id' => 'int',
		'name' => 'varchar',
		'username' => 'varchar',
		'password' => 'varchar',
	];

	public static $defaults = [
		'id' => '',
		'name' => '',
		'username' => '',
		'password' => '',
	];
}