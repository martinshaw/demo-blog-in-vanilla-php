<?php
namespace App\Models;

class Model {
	protected $id = 0;
    protected $properties = [];

    public static $table = '';
    public static $fields = [];
    public static $defaults = [];
		
	/**
	 * __construct
	 *
	 * @param  array $properties
	 * @return Model
	 */
	public function __construct($properties) {
		$this->properties = static::$defaults;
		$this->id = isset($properties['id']) ? $properties['id'] : 0;
		foreach ($properties as $property => $value) {
            if (in_array($property, array_keys(static::$fields))) {
                $this->properties[$property] = $value;
            }
		}
		return $this;
	}
	
	/**
	 * find
	 *
	 * @param  int $id
	 * @return bool|Model
	 */
	public static function find(int $id) {
		$record = \App\Database::getInstance()->selectSingular($id, static::$table);
		return $record === false ? false : new static($record);
	}

	public static function all() {
		$records = \App\Database::getInstance()->selectAll(static::$table);
		return array_map(function ($record) {
			return new static($record);
		}, $records);
	}

	public static function paginate($currentPage, $orderBy) {
        $perPage = 6;
        $count = static::count();
		$pageCount = $count / $perPage;
		
		$records = \App\Database::getInstance()->selectRange($perPage * ($currentPage -1), $perPage, static::$table, $orderBy);

		return [
            'items' => array_map(function ($record) {
				return new static($record);
			}, $records),
            'pagination' => [
                'page' => $currentPage,
                'next' => $currentPage < $pageCount ? '?page=' . ($currentPage +1) : null,
                'prev' => $currentPage > 1 ? '?page=' . ($currentPage -1) : null,
            ]
		];
	}

	public static function where(array $filters) : array {
		$records = \App\Database::getInstance()->where($filters, static::$table);
		return array_map(function ($record) {
			return new static($record);
		}, $records);
	}

	public static function count() {
		$count = \App\Database::getInstance()->count(static::$table);
		return intval($count);
	}

	public function save() {
		return empty($this->id) ? $this->insert() : $this->update();
	}

	public function insert() {
		$id = \App\Database::getInstance()->insert($this->getProperties(), static::$table);
		
		$record = \App\Database::getInstance()->selectSingular($id, static::$table);
		return $record === false ? false : new static($record);
	}

	public function update($changes) {
		$id = \App\Database::getInstance()->update($changes, $this->id, static::$table);
		
		$record = \App\Database::getInstance()->selectSingular($id, static::$table);
		return $record === false ? false : new static($record);
	}

	public function delete() {
		return \App\Database::getInstance()->delete($this->getProperties()['id'], static::$table);
	}
    
    /**
     * toJson
     *
     * @return array
     */
    public function toJson() {
		return $this->properties;
	}
	
	/**
	 * getProperties
	 *
	 * @return array
	 */
	public function getProperties() {
		return $this->properties;
	}
}