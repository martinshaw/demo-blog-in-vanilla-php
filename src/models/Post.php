<?php
namespace App\Models;

class Post extends Model
{
	public static $table = 'posts';

	public static $fields = [
		'id' => 'int',
		'title' => 'varchar',
		'content' => 'longtext',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'user_id' => 'integer',
	];

	public static $defaults = [
		'id' => '',
		'title' => '',
		'content' => '',
		'created_at' => '',
		'updated_at' => '',
		'user_id' => 0,
	];

	public function getDetails() {
		$user = $this->getUser();
		$username = $user === false ? 'Non-existent User' : $user->getProperties()['name'];
		return 'Published by ' . $username . ' at ' . $this->getFormattedCreatedAtDate();
	}

	public function getUser() {
		return \App\Models\User::find($this->getProperties()['user_id']);
	}

	public function getFormattedCreatedAtDate() {
		return (new \DateTime($this->getProperties()['created_at']))
			->format('g:ia \o\n l jS F Y');
	}

	public static function sanitiseContent($content) {
		$content = str_replace("\n", '<br/>', $content);
		$safeHtmlTags = array('b', 'i', 'u', 'br', 'img', 'a', 'p', 'ul', 'li');
		return preg_replace("/&lt;\s*(\/?\s*)(".implode("|", $safeHtmlTags).")(\s?|\s+[\s\S]*?)(\/)?\s*&gt;/", "<$1$2$3$4>", htmlspecialchars($content));
	}
}