<?php
namespace App;

class Router extends Singleton
{
	use FetchesArray;

	protected $path = '../routes.php';

	protected $routes;
	protected $params = [];

	public function __construct() {
		if (file_exists($this->path) === false) {
			$exception = new \Exception('There is no routes file. You may need to copy the \'routes.example.php\' file as \'routes.php\'');
			echo $exception->getMessage();
			throw $exception;
		}
		$this->routes = $this->fetchArray($this->path);
	}

	public function dispatch() {
		$route = $this->findRouteByRequest(Request::getInstance());
		try {
			return $this->handleRoute($route, $this->params);
		} catch (\Exception $exception) {
			return $this->handleRoute($this->getExceptionRoute(), [$exception]);
		}
	}

	protected function findRouteByRequest($request) {
		foreach ($this->getRouteUrls() as $routeUrl) {
			$matches = [];
			
			preg_match_all(
				$this->formatRouteUrlForRegex($routeUrl),
				$request->getPath()['path'],
				$matches,
				PREG_SET_ORDER
			);
			if (isset($matches[0])) {
				$match = $matches[0];
				array_shift($match);
				$this->params = $match;

				return is_callable($this->routes[$routeUrl]) ? 
					$this->routes[$routeUrl] :
					$this->routes[$routeUrl][Request::getInstance()->getMethod()];
			}
		}
		return $this->getNotFoundRoute();
	}

	protected function handleRoute($route, $args = []) {
		$response = call_user_func_array($route, $args);
		if ($response instanceof \App\Responses\Response === false) {
			throw new \Exception('The chosen route callback must return a Response class');
		}
		return $response;
	}

	protected function formatRouteUrlForRegex($routeUrl) {
		$routeUrl = str_replace('/', '\\/', $routeUrl);
		$routeUrl = str_replace('{number}', '([0-9]+)', $routeUrl);
		$routeUrl = str_replace('{alpha}', '([a-zA-Z]+)', $routeUrl);
		$routeUrl = str_replace('{any}', '([a-zA-Z0-9]+)', $routeUrl);
		return '/^'.$routeUrl.'$/i';
	}

	protected function getRouteUrls() {
		return array_keys($this->routes);
	}

	public function getParams() {
		return $this->params;
	}

	public function getRouteUrl() {
		return \App\Request::getInstance()->getPath()['path'];
	}

	protected function getNotFoundRoute() {
		return function () {
			return new \App\Responses\ViewResponse('404.php');
		};
	}

	protected function getExceptionRoute() {
		return function ($exception) {
			return new \App\Responses\ViewResponse('500.php', [
				'exception' => $exception,
				'debug' => Config::getInstance()->get('debug'),
			]);
		};
	}
}