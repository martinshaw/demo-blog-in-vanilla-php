<?php
namespace App;

trait FetchesArray {
    function fetchArray($in) {
        return (is_file($in)) ? include $in : false;
    }
}