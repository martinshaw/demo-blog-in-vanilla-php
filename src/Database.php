<?php
namespace App;

class Database extends Singleton {
	protected $is_error = false;
	public $error_msg = "";

	protected $conn = "";

	// Create connection
	private function makeConnection(){
		$config = Config::getInstance()->get('database');
		$this->conn = new \mysqli($config['hostname'], $config['username'], $config['password'], $config['database']);

		// Check connection
		if ($this->conn->connect_error) {
			$this->is_error = true;
			$this->error_msg = $this->conn->connect_error . "";
			throw new \Exception('Cannot connect to database with the provided credentials');
		} 
		return true;		
	}

	private function query($sql) {
		$this->is_error = false;
		$result = $this->conn->query($sql);
		if (empty($this->conn->error) === false) {
			$this->is_error = true;
			throw new \Exception('Database Exception: ' . $this->conn->error);
		}
		return $result;
	}

	public function selectSingular($id, $table){
		$this->makeConnection();

		$sql = "SELECT * FROM " . $table . " where id='" . mysqli_real_escape_string($this->conn, $id) . "' limit 1";
		$result = $this->query($sql);

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$record = $row;
			}
		}
		$this->conn->close();
		return isset($record) ? $record : false;
	}

	public function selectAll($table, $orderBy = ['id', 'ASC']){
		$this->makeConnection();

		$sql = "SELECT * FROM {$table} ORDER BY `{$orderBy[0]}` {$orderBy[1]}";
		$result = $this->query($sql);

		$records = [];
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$records[] = $row;
			}
		}
		$this->conn->close();
		return $records;
	}

	public function selectRange($offset, $limit, $table, $orderBy = ['id', 'ASC']){
		$this->makeConnection();

		$sql = "SELECT * FROM {$table} ORDER BY `{$orderBy[0]}` {$orderBy[1]} LIMIT {$limit} OFFSET {$offset}";
		$result = $this->query($sql);

		$records = [];
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$records[] = $row;
			}
		}
		$this->conn->close();
		return $records;
	}

	public function where(array $filters, string $table, $orderBy = ['id', 'ASC']) : array {
		$this->makeConnection();

		$filters = array_map(
			function($key) use ($filters) {
				list($fieldName, $fieldOperator) = explode(' ', $key);
				$value = mysqli_real_escape_string($this->conn, $filters[$key]);
				$value = is_string($value) ? "'$value'" : $value;
				return "`{$fieldName}` {$fieldOperator} {$value}";
			},
			array_keys($filters)
		);
		$querySuffix = (empty($filters) ? '' : ' WHERE ') . implode(' AND ', $filters);

		$sql = "SELECT * FROM {$table} {$querySuffix} ORDER BY `{$orderBy[0]}` {$orderBy[1]}";
		$result = $this->query($sql);

		$records = [];
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$records[] = $row;
			}
		}
		$this->conn->close();
		return $records;
	}

	public function count($table) {
		$this->makeConnection();

		$sql = "SELECT count(*) as total FROM " . $table;
		$result = $this->query($sql);

		$data = mysqli_fetch_assoc($result);
		return $data['total'];
	}

	public function insert($properties, $table) {
		$this->makeConnection();

		$fieldNames = implode(',', array_keys($properties));
		$values = implode(',', array_map(function ($v) { return '\''.$v.'\''; }, array_values($properties)));
		
		$sql = "INSERT INTO {$table} ({$fieldNames}) values ({$values})";
		$result = $this->query($sql);

		return $this->conn->insert_id;
	}

	public function update($changedProperties, $id, $table) {
		$this->makeConnection();

		$fields = [];
		foreach ($changedProperties as $field => $value) {
			$value = mysqli_real_escape_string($this->conn, $value);
			$fields[] = $field . '=' . (is_string($value) ? "'{$value}'" : $value);
		}
		$fields = implode(', ', $fields);
		
		$sql = "UPDATE {$table} SET {$fields} where `id`={$id}";
		$result = $this->query($sql);

		return $this->conn->insert_id;
	}

	public function delete($id, $table) {
		$this->makeConnection();
		
		$sql = "DELETE FROM {$table} where `id` = {$id}";
		$result = $this->query($sql);
		
		return $result;
	}
}

