<?php
namespace App\Responses;

abstract class Response
{
	protected $content = '';
	protected $statusCode = 200;
	protected $headers = [];

	public function __construct($content, $statusCode = 200, $headers = []) {
		$this->content = $content;
		$this->statusCode = $statusCode;
		$this->headers = $headers;
	}
	
	public function perform() {
		echo $this->content;
	}

	public static function handleResponse($response) {
		http_response_code($response->statusCode);
		foreach ($response->headers as $title => $value) {		
			header("$title: $value");
		}
		ob_start();
		$response->perform();
		ob_end_flush();
	}
}