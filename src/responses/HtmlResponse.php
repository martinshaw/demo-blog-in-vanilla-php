<?php
namespace App\Responses;

class HtmlResponse extends Response
{
	protected $path = '';
	protected $fullPath = '';

	public function __construct($path, $statusCode = 200, $headers = []) {
		$this->path = $path;
		$this->fullPath = \App\Application::getInstance()->getAppPath() . $path;

		if (is_file($this->fullPath)) {
			return parent::__construct(file_get_contents($this->fullPath), $statusCode, $headers);
		}
		throw new \Exception('Cannot find HTML file at the provided path');
	}
}