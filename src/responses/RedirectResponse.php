<?php
namespace App\Responses;

class RedirectResponse extends Response
{
	public function __construct($url, $statusCode = 200, $headers = []) {
		$headers = array_merge(['Location' => $url], $headers);
		return parent::__construct('', $statusCode, $headers);
	}
}