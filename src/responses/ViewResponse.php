<?php
namespace App\Responses;

class ViewResponse extends Response
{
	protected $path = '';
	protected $fullPath = '';
	protected $data = [];

	public function __construct($path, $data = [], $statusCode = 200, $headers = []) {
		$this->path = $path;
		$this->fullPath = \App\Application::getInstance()->getAppPath() . '/views/' . $path;
		$this->data = $data;

		if (is_file($this->fullPath)) {
			return parent::__construct($this->fullPath, $statusCode, $headers);
		}
		throw new \Exception('Cannot find PHP view file at the provided path');
	}

	public function perform() {
		$section = function ($filePath) {
			$data = $this->data;
			$auth = \App\Authentication::getInstance();
			$user = $auth->getUser();
			include(\App\Application::getInstance()->getAppPath() . '/views/sections/' . $filePath);
		};

		$data = $this->data;
		$auth = \App\Authentication::getInstance();
		$user = $auth->getUser();
		include_once($this->fullPath);
	}
}