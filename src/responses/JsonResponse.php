<?php
namespace App\Responses;

class JsonResponse extends Response
{
	protected $array = [];

	public function __construct($array, $statusCode = 200, $headers = []) {
		$this->array = $array;
		return parent::__construct(
			json_encode(["data" => $array]),
			$statusCode,
			array_merge(['Content-Type' => 'application/json'], $headers)
		);
	}
}