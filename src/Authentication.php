<?php
namespace App;

use App\Models\User;

class Authentication extends Singleton
{
	protected $user = null;
	protected $redirectTo = '/';

	public function __construct() {
		if ($this->isSignedIn()) {
			return true;
		}
		// If user not already set before instanciation, attempt to restore auth state from session
		if (isset($_SESSION['user'])) {
			$user = User::find(intval($_SESSION['user']));
			if ($user === false) {
				return true;
			}
			$this->setUser($user);
		}
	}

	public function isSignedIn() : bool {
		return is_null($this->user) === false;
	}

	public function getUser() {
		return $this->user;
	}

	public function setUser(User $user) : User {
		$_SESSION['user'] = $user->getProperties()['id'];
		$this->user = $user;
		return $this->user;
	}

	public function attempt(string $username, string $password) : bool {
		$users = User::where(['username =' => $username]);
		if (empty($users)) {
			return false;
		}

		$hash = $users[0]->getProperties()['password'];
		if (password_verify($password, $hash) !== true) {
			return false;
		}

		$this->setUser($users[0]);
		return true;
	}

	public function forget() : bool {
		unset($_SESSION['user']);
		$this->user = null;
		return true;
	}
	
	public function redirect() : \App\Responses\RedirectResponse {
		return new \App\Responses\RedirectResponse($this->redirectTo);
	}
}