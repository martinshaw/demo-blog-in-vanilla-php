<?php 

return [

    // Handles index url and redirects to paginated list of Posts
    '/' => function () {
        return new \App\Responses\RedirectResponse('/posts');
    },

    // Displays information about this project
    '/about' => function () {
        return new \App\Responses\ViewResponse('about.php');
    },

    '/posts' => [

        // Displays paginated list of Posts
        'GET' => function () {
            $paginatedPosts = \App\Models\Post::paginate(isset($_GET['page']) ? intval($_GET['page']) : 1, ['created_at', 'DESC']);
            
            $monthlyPosts = [];
            foreach (\App\Models\Post::where(['created_at >' => (new \DateTime)->modify('-1 year')->format('Y-m-d H:i:m')]) as $post) {
                $date = strtotime($post->getProperties()['created_at']);
                $monthlyPosts[date('F', $date) . ' ' . date('Y', $date)][] = $post;
            }

            return new \App\Responses\ViewResponse('index.php', array_merge($paginatedPosts, ['monthlyPosts' => $monthlyPosts] ));
        },

        // Stores a new Post
        'POST' => function () {
            if (\App\Authentication::getInstance()->isSignedIn() === false) {
                return \App\Authentication::getInstance()->redirect();
            }

            if (empty($_POST['title']) or empty($_POST['content'])) {
                return new \App\Responses\ViewResponse('post-new.php', [
                    'errors' => [
                        'You must provided a valid title and contents for a new Post'
                    ]
                ]);
            }
            $post = new \App\Models\Post([
                'title' => $_POST['title'],
                'content' => base64_encode(\App\Models\Post::sanitiseContent($_POST['content'])),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'user_id' => \App\Authentication::getInstance()->getUser()->getProperties()['id'],
            ]);
            $post = $post->save();
            
            return new \App\Responses\RedirectResponse('/posts/' . $post->getProperties()['id']);
        }

    ],

    // Displays the Create New Post form page
    '/posts/new' => function () {
        if (\App\Authentication::getInstance()->isSignedIn() === false) {
            return \App\Authentication::getInstance()->redirect();
        }

        return new \App\Responses\ViewResponse('post-new.php');
    },

    // Displays singular Post
    '/posts/{number}' => [
        'GET' => function ($id) {
            $post = \App\Models\Post::find($id);
    
            if ($post === false) {
                return \App\Router::getNotFoundRoute()();
            }
            return new \App\Responses\ViewResponse('post.php', ['post' => $post]);
        },
        'PUT' => function($id) {
            if (\App\Authentication::getInstance()->isSignedIn() === false) {
                return \App\Authentication::getInstance()->redirect();
            }

            $post = \App\Models\Post::find($id);
            $post->update([
                'title' => $_POST['title'],
                'content' => base64_encode(\App\Models\Post::sanitiseContent($_POST['content'])),
            ]);
            
            return new \App\Responses\RedirectResponse('/posts/' . $post->getProperties()['id']);
        }
    ],

    // Displays the Edit Existing Post form page
    '/posts/{number}/edit' => function ($id) {
        if (\App\Authentication::getInstance()->isSignedIn() === false) {
            return \App\Authentication::getInstance()->redirect();
        }

        $post = \App\Models\Post::find($id);

        if ($post === false) {
            return \App\Router::getNotFoundRoute()();
        }

        return new \App\Responses\ViewResponse('post-edit.php', ['post' => $post]);
    },

    // Deletes a singular Post
    '/posts/{number}/delete' => function ($id) {
        if (\App\Authentication::getInstance()->isSignedIn() === false) {
            return \App\Authentication::getInstance()->redirect();
        }

        $post = \App\Models\Post::find($id);

        if ($post === false) {
            return \App\Router::getNotFoundRoute()();
        }

       $post->delete($id);
        return new \App\Responses\RedirectResponse('/posts');
    },

    // Displays Posts by month and year
    '/posts/{alpha}/{number}' => function ($month, $year) {
        $startDate = new \DateTime("{$year}-{$month}-01 00:00:00");
        $endDate = (new \DateTime("{$year}-{$month}-01 00:00:00"))->add(new \DateInterval('P1M'));
        $posts = \App\Models\Post::where(['created_at >=' => $startDate->format('Y-m-d H:i:m'), 'created_at <' => $endDate->format('Y-m-d H:i:m'), ]);

        $monthlyPosts = [];
        foreach (\App\Models\Post::where(['created_at >' => (new \DateTime)->modify('-1 year')->format('Y-m-d H:i:m')]) as $post) {
            $date = strtotime($post->getProperties()['created_at']);
            $monthlyPosts[date('F', $date) . ' ' . date('Y', $date)][] = $post;
        }

        if (empty($posts)) {
            return \App\Router::getNotFoundRoute()();
        }
        return new \App\Responses\ViewResponse('index.php', ['items' => $posts, 'monthlyPosts' => $monthlyPosts]);
    },

    '/auth/signin' => [

        // Displays Sign In form page
        'GET' => function () {
            return new \App\Responses\ViewResponse('auth-signin.php', ['redirectTo' => \App\Request::getInstance()->getPreviousUrl()['path']]);
        },

        // Authenticates user
        'POST' => function () {
            $attempt = \App\Authentication::getInstance()->attempt($_POST['username'], $_POST['password']);
            return ($attempt) ?
                new \App\Responses\RedirectResponse($_POST['redirectTo']) :
                new \App\Responses\ViewResponse('auth-signin.php', ['redirectTo' => $_POST['redirectTo'], 'errors' => ['The provided Username and Password is not valid']]);
        }

    ],

    // Deauthenticates user
    '/auth/signout' => function () {
        \App\Authentication::getInstance()->forget();
        return new \App\Responses\RedirectResponse(\App\Request::getInstance()->getPreviousUrl()['path']);
    }

];